	.data
msg1: .asciiz "Please enter first non-negative integer: "
msg2: .asciiz "Please enter second non-negative integer: "
msg3: .asciiz "The input numbers is negative, please prompt agian\n"
msg4: .asciiz "The result of Ackermann's function is: "

	.text
	.globl main
main:
	add $29, $29, -4    #update stack pointer
	sw $31, 4($29) 

command:
	li $2,4			#prompt user for an integer
	la $4,msg1
	syscall 

	li $2,5			#store the input integer into $t0
	syscall
	move $t0,$2

	li $2,4			#prompt the user for another integer
	la $4,msg2
	syscall

	li $2,5			#store the input integer into $t1
	syscall
	move $t1,$2

	slt $t2,$t0,$0		# if $t0<0, store 1 into $t2, otherwise, store 0
	slt $t3,$t1,$0		# if $t1<0, store 1 into $t3
	add $t4,$t2,$t3	
	
	beq $t4,$0,cal	#if $t2+$t3 = 0 mean both intergers is larger than 0

	li $2,4			#else ask user to enter another two integers until 
	la $4,msg3			#they are both non-negative
	syscall
	j command

cal:
	move $4,$t0		#pass the first integer in $4
	move $5,$t1		#pass the second integer in $5

	jal Ackermann		#jump to the procedure Ackermann

	move $t0,$2

	li $2,4			#print the description message
	la $4,msg4
	syscall

	li $2,1			#print the return value
	move $4,$t0
	syscall

	lw $31, 4($29)		# get the return address
	addu $29, $29, 4
	jr $31				#return from main

Ackermann:
	subu $29, $29, 4	# update stack pointer
	sw $31, 4($29)		# store return address into stack
	addu $s3,$s3,1
	beq $4,$0,end		#if x=0, end the recursive call
	beq $5,$0,elseIf	#if y=0, jump to 'elseIf' branch
	subu $29, $29, 4	# update stack pointer		
	sw $4, 4($29)	
	sub $5, $5, 1		#y-1
	jal Ackermann
	lw $4, 4($29)		# load first parameter stored		
	addu $29, $29, 4
	sub $4, $4, 1		#x-1
	move $5, $2
	jal Ackermann
	lw $31, 4($29)		# get the return address
	addu $29, $29, 4	# update stack pointer
	jr $31	
end:
	addi $2, $5, 1 	# if x = 0, return y + 1
	lw $31, 4($29)		# get the return address
	addu $29, $29, 4	# update the stack	
	jr $31
elseIf:
	sub $4, $4, 1		# x-1		
	li $5, 1		# will call with y = 1
	jal Ackermann		# call Ackermann
	lw $31, 4($29)		# get the return address
	addu $29, $29, 4	# update the stack
	jr $31	