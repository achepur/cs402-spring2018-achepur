.data 0x10000000
msg1: .asciiz "enter an float number: "
.text 
.globl main

main: addu $16, $31, $0 # save $31 in $16 
li $2, 4 # system call for print_str 
la $a0, msg1 # address of string to print 
syscall

# now get an float from the user 
li $2, 6 # system call for read_float 
syscall # the float placed in $v0
mov.s $f1, $f0 # move input value to print in $f1

# print the result 
li $2, 2 # system call for print_float  
mov.s $f12, $f0 # move number to print in $f12
syscall

# restore now the return address in $ra and return from main 
addu $31, $0, $s0 # return address back in $31 
jr $31 # return from main
