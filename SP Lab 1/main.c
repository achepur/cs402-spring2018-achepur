#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "readfile.h"
#include "define.h"

//Declarations
void readFile();
void printTabularData();
int binarySearch(int low,int high,int id);
void searchEmployeeByID();
void searchEmployeeByLastname();
void addEmployee();
void printMenu();


//Main calling all the methoods
int main(int a, char *argv[])
{
	if(a!=2) {
		printf("Filename Error\n");
		return 0;
	}

	if(!openFile(argv[1])){
		printf("Cannot open the file %s\n",argv[1]);
		return 0;
	}
	readFile();

	int i = 0;
	do{
		printMenu();
		int option = scanf("%d",&i);
		while(!option){
			int c;
			while ((c=getchar()) != '\n' && c != EOF);
			printf("Please Enter a digit  \n");
			printMenu();
			option = scanf("%d",&i);
		}
        if (i == 1){
            printTabularData();

        }
        else if (i == 2){
            searchEmployeeByID();

        }
        else if (i == 3){
            searchEmployeeByLastname();

        }
        else if (i == 4){
            addEmployee();

        }
        else if (i == 5){
            printf("goodbye!! . .\n");

        }
        else
            printf(" %d is not between 1 and 5, Please Enter a digit from 1 to 5 \n",i);


    }while(i != 5);

	closeFile();
	return 0;
}

struct Employee
{
   int sixDigitID;
   char firstName[NAMEMAXLENGTH];
   char lastName[NAMEMAXLENGTH];
   int salary;
};

struct Employee E[EMPLOYEEMAXNUM];

// Function to read file with calls from readfile.c
void readFile(){
	int sixDigitID = 1;
	int salary;
        char firstName[NAMEMAXLENGTH],lastName[NAMEMAXLENGTH];
	
	int i=0;
	while(i<EMPLOYEEMAXNUM){
		if(read_int(&sixDigitID)==0)
			E[i].sixDigitID = sixDigitID;
		else
			break;

		if(read_string(firstName)==0)
			strcpy(E[i].firstName,firstName);
		else
			break;

		if(read_string(lastName)==0)
			strcpy(E[i].lastName,lastName);
		else
			break;

		if(read_int(&salary)==0)
			E[i].salary = salary;
		else
			break;
        i++;
		}

}


//Function to traverse the Employees array and print them in a tabular format
void printTabularData(){
	printf("\nNAME\t\t\t\tSALARY\tID\n");
	printf("---------------------------------------------------------------\n");
        int i=0;
	while (i<EMPLOYEEMAXNUM){
		if(E[i].sixDigitID != 0){
			printf("%s\t%-10s\t\t%d\t%d\n", E[i].firstName,E[i].lastName,E[i].salary,E[i].sixDigitID);
		}else
			break;

        i++;
	}
    printf("---------------------------------------------------------------\n");
	printf("Number of Employee (%d)\n\n",i);
}

// Binary search Function which is used in searching Employee by their ID
int binarySearch(int low,int high,int id){
	//if not found, return -1
	if(low > high)
        return -1;

    //middle of the array
        int mid = low + (high - low)/2;
    // if mid = id , return current 
        if(E[mid].sixDigitID == id)
            return mid;
    // if mid > id, run the function for lower half of the array
        if (E[mid].sixDigitID > id ||E[mid].sixDigitID == 0 ) 
			return binarySearch(low, mid-1, id);

		// Else the element can only be present
		// in higher subarray
		return binarySearch(mid+1, high, id);

    
}

// Function to Search Employee by ID using Binary Search
void searchEmployeeByID(){
	int id;
	printf("Enter a 6 digit employee id : ");
	scanf("%d",&id);
    
        int n = sizeof(E)/ sizeof(E[0]);
        
	//call binarySearch to find the id from the E
	int found = binarySearch(0,n-1,id);

	if(found > 0){
		printf("\nNAME\t\t\t\tSALARY\tID\n");
		printf("---------------------------------------------------------------\n");
		printf("%s\t%-15s\t\t%d\t%d\n", E[found].firstName,E[found].lastName,E[found].salary,E[found].sixDigitID);
		printf("---------------------------------------------------------------\n\n");
	}
    else
		printf("Employee with id %d not found\n\n",id);

}

//Function to search employee by his lastname
void searchEmployeeByLastname(){
	char lastName[NAMEMAXLENGTH];
	printf("Enter last name of the Employee: ");
	scanf("%s",&lastName);
	int i = 0;
    while(i<EMPLOYEEMAXNUM){
		if(E[i].sixDigitID != 0){
			if(!strcmp(E[i].lastName, lastName)){
				printf("\nNAME\t\t\t\tSALARY\tID\n");
				printf("---------------------------------------------------------------\n");
				printf("%s\t%-15s\t\t%d\t%d\n", E[i].firstName,E[i].lastName,E[i].salary,E[i].sixDigitID);
				printf("---------------------------------------------------------------\n\n");
				return;
			}
		}else{
			printf("Employee with last name %s not found \n\n",lastName);
			break;
		}
        i++;
	}
}

// Function to add employee to the array
void addEmployee(){
	int sixDigitID = 1;
	char firstName[NAMEMAXLENGTH],lastName[NAMEMAXLENGTH];
    	int salary;
    	int confirm;
	
	printf("Enter the first name of the employee: ");
	scanf("%s",firstName);
	printf("Enter the last name of the employee: ");
	scanf("%s",lastName);
	printf("Enter a 6 digit employee id num: ");
	scanf("%d",&sixDigitID);

	while(sixDigitID<100000 ||sixDigitID>999999){
		printf("Id should be 6 digits.\nEnter a 6 digit employee id num: ");
		scanf("%d",&sixDigitID);
	}
	printf("Enter employee's salary from 30000 to 150000: ");
	scanf("%d",&salary );
	while(salary<30000 || salary>150000){
		printf("The salary should be between 30000 and 150000.\nEnter employee's salary from 30000 to 150000): ");
		scanf("%d",&salary );
	}
	printf("Do you want to add the employee?\n");
	printf("%s %s, salary: %d, id: %d\n",firstName,lastName,salary,sixDigitID);
	printf("Enter 1 for yes, 0 for no: ");
	scanf("%d",&confirm);
	if(confirm == 1){
        printf("Adding new Employee . .");
        int i = 0;
		while(i<EMPLOYEEMAXNUM){
                //insert the new employee in the array
			if(E[i].sixDigitID == 0){
				E[i].sixDigitID = sixDigitID;
				strcpy(E[i].firstName,firstName);
				strcpy(E[i].lastName,lastName);
				E[i].salary = salary;
				break;
            }
           i++; 
		}
	}
	printf("\n");
}

//Function to print the menu
void printMenu(){
	printf("Employee DB Menu:\n");
	printf("----------------------------------\n");
	printf("1.Print the Database\n");
	printf("2.Lookup employee by ID\n");
	printf("3.Lookup employee by last name\n");
	printf("4.Add an Employee\n");
	printf("5.Quit\n");
	printf("----------------------------------\n");
	printf("Enter your choice:");
}


