		.data 0x10010000
my_array:	.space 40

		.text
		.globl main
main:	addu $16, $31, $0 # save $31 in $16

		li $t0, 7			# $t0 = 7 = j
		li $t1, 10 			# $t1 = 10
		li $t5, 0 			# $t5 = 0 = i
		la $a0, my_array	#read my_array to $a0

Loop:	ble $t1,$t5,Exit 	# exit of 10<=$t5
		addi $t5,$t5,1		# i = i + 1
		sw $t0, 0($a0)		# store j in my_array
		addi $a0, $a0, 4	# $a0 <= $a0 + 4
		addi $t0, $t0, 1		# var1 = var1+1
		j Loop
		
# restore now the return address in $ra and return from main
Exit:	addu $31, $0, $16 	# return address back in $31
		jr $31 				# return from main