		.data 0x10010000
msg1: 	.asciiz "Please enter two integer numbers: "
msg2: 	.asciiz "I'm far away"
msg3: 	.asciiz "I'm nearby"

		.text
		.globl main
main:	addu $16, $31, $0 # save $31 in $16

		li $v0, 4			# system call for print_str
		la $a0, msg1		# address of string to print
		syscall

		li $v0, 5 			# system call for read_int
		syscall
		addu $t0, $v0, $0	# first integer in $t0
		li $v0, 5 			# system call for read_int
		syscall
		addu $t1, $v0, $0 	#second integer in $t1

# comparision operator 
		bne $t0, $t1, Nearby
		j Far
Nearby:	li $v0, 4 			# system call for print_string
		la $a0, msg3
		syscall
		j Exit

	.text 0x00410000		#Nearby address
Far:	li $v0, 4
		la $a0, msg2
		syscall
		j Exit
		
# restore now the return address in $ra and return from main
Exit:	addu $31, $0, $16 	# return address back in $31
		jr $31 				# return from main