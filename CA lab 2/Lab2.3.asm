		.data 0x10010000
var1:	.word 1
var2: 	.word 2
var3: 	.word 3
var4: 	.word 4
first:	.byte 'a'
last:	.byte 'c'

		.text
		.globl main
main:	addu $16, $31, $0	# save $31 in $16

		lui $at, 4097
		lw $9, 0($at)
		lui $at, 4097
		lw $10, 4($at)
		lui $at, 4097
		lw $11,8($at)
		lui $at, 4097
		lw $12,12($at)

#swap values

		sw $12, 0($at)
		sw $11, 4($at)
		sw $10, 8($at)
		sw $9, 12($at)

# restore now the return address in $ra and return from main
		addu $31, $0, $16 # return address back in $31
		jr $31 # return from main