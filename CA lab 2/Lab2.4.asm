		.data 0x10010000
var1:	.word 79
var2: 	.word 26
	.extern 	ext1 4
	.extern 	ext2 4

		.text
		.globl main
main:	addu $16, $31, $0 # save $31 in $16

		lw $9, var1 	#load value of var1 in $t1
		lw $19, var2	#load value of var2 in $t2

		sw $9, ext1			#store value in $t1 into ext1
		sw $9, -32764($gp)		#store value in $t1 into ext2

# restore now the return address in $ra and return from main
		addu $31, $0, $16 # return address back in $31
		jr $31 # return from main