#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "define.h"


FILE *filePointer;

// Function to open file
int openFile(char *filename){
	filePointer = fopen (filename, "r+");
        if (filePointer) 
		return 1;
	else
		return 0;

}

//Function to read float , returns 0 on success, -1 on EOF 
int read_float(float *f){
	if(fscanf(filePointer, "%f", f)==1)
		return 0;
	else
		return -1;
	
}

//Function to read integer , returns 0 on success, -1 on EOF 
int read_int(int *x){
	if(fscanf(filePointer, "%d", x)==1)
		return 0;
	else
		return -1;
	
}

//Function to read string , returns 0 on success, -1 on EOF 
int read_string(char s[NAMEMAXLENGTH]){
	if(fscanf(filePointer, "%s", s)==1)
		return 0;
	else
		return -1;
	
}


//Function to close file 
void closeFile(){
	fclose(filePointer);
}