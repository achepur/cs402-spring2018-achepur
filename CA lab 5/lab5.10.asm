	.data 0x10000000
	.align 0

ch1: 	.byte 'a'
word1: 	.word 0x89abcdef
ch2: 	.byte 'b'
word2:	.word 0
	
	.text
	.globl main

main:
	la $a0, word1	#load the address of word1 to $t0
	la $a1, word2   #load the address of word2 to $t1
	
	lwr $t0, 0($a0) #load the last 6 bytes 
	lwl $t0, 3($a0)	#load the first 2 bytes to t1

	swr $t0, 0($a1)	#save the last 6 bytes
	swl $t0, 3($a1) #save the first 2 bytes

	jr $ra
	