.data 0x10000000
word1: 	.word 0x89abcdef 	# reserve space for a word

		.text
		.globl main

main:	la $a0, word1

		lwr $t4, 0($a0)
		lwr $t5, 1($a0)
		lwr $t6, 2($a0)
		lwr $t7, 3($a0)
		
		jr $ra