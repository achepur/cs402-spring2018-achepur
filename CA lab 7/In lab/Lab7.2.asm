    .data
message: .asciiz "Hello world\n"
    .align 4
buffer:  .space 256
t_in:   .word  0
t_out:  .word  0

    .text
	.globl __start
__start:
	mfc0 $t0, $12       # load status register    
    li   $t0,0x400      # enable interrupts
	mtc0 $t0, $12    

	li $t0,0xffff0008 	# load address of transmitter control
	lw  $t1,0($t0)	# load transmitter control state
	ori $t1,$t1,2	# enable interrupts
	sw  $t1,0($t0)	# save transmitter control state

    li $s0,100
loop:

    la $a0,message
    jal PrintString
    
    addi $s0,$s0,-1
    bnez $s0,loop    

doprint:
    lw   $t1,t_in       # wait until the buffer is empty
    lw   $t2,t_out
    bne  $t1,$t2,doprint   
    
    li $v0,10
    syscall

PrintString:
  
ldloop:
    lb  $t0,($a0)   # load char to send
    beqz $t0,ps_end

    lw   $t1,t_in       # save char at t_in position in buffer 
    lw   $t2,t_out
    addi $t3,$t1,1      # move to next position in t_in
    andi $t3,$t3,255    # adjust to buffer size
    bne  $t2,$t3,continue   # if it's not full, continue
wait:                   
	mfc0 $t4, $12       # load status register    
    ori  $t4,$t4,1      # enable interrupts
	mtc0 $t4, $12    

    lw   $t2,t_out
    beq  $t2,$t3,wait   # if it's full, wait
continue:
    la   $t2,buffer
    add  $t2,$t2,$t1
    sb   $t0,($t2)
    sw   $t3,t_in       

	mfc0 $t0, $12       # load status register    
    ori  $t0,$t0,1      # enable interrupts
	mtc0 $t0, $12    
   
next:
    addi $a0,$a0,1  # advance to next char in string
    j   ldloop
ps_end:
    jr $ra
           

	.kdata
s1:	.word 0
s2:	.word 0
s3:	.word 0
s4:	.word 0
s5:	.word 0

	.ktext 0x80000180
	.set noat
	move $k1 $at	# Save $at
	.set at
	sw $v0 s1
	sw $a0 s2
	sw $t0 s3
	sw $t1 s4
	sw $ra s5

	mfc0 $k0 $13	    # Cause
	srl  $t0,$k0, 2     # Extract exception code from $k0, bits 2-5
	andi $t0,$t0, 0x1f
	bnez $t0,ret  		# if not zero is not an  interrupt
    addu $0 $0 0

    lw  $t0,t_out
    lw  $t1,t_in
    beq $t0,$t1,ret2     # if the buffer is empty, exit

    la  $t1,buffer
    add $t0,$t0,$t1
    lb  $a0,($t0)       # load char from buffer
    jal PutChar         #  print it on display
    lw  $t0,t_out
    addi $t0,$t0,1      # advance t_out
    andi $t0,$t0,255
    sw $t0,t_out        # update t_out
    b  ret
ret2:
	mfc0 $v0, $12       # load status register    
    li   $a0,1
    not  $a0,$a0
    and  $v0,$v0,$a0    # disable interrupts
	mtc0 $v0, $12
ret:	
	mtc0 $0, $13	# Clear Cause register
    lw $v0 s1
	lw $a0 s2
	lw $t0 s3
	lw $t1 s4
	lw $ra s5
	mfc0 $k0 $14	# EPC
	.set noat
	move $at $k1	# Restore $at
	.set at
    eret

PutChar:
	li $t0,0xffff0000	# load start address of I/O registers
	lw  $t1,8($t0)		# read terminal status
	andi $t1,$t1,1		# see if ready to print
	beq $t1,$0,pc_end   # if not end
	sw  $a0,12($t0)		# send character to display
pc_end:
	jr  $ra

