#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Define the length of the initial array
#define ARRAYLENGTH 20


//Update the size of the array
float* updateArraySize(float *prev,int size){
	float *updateArray;
	//space for new array using malloc
	updateArray = (float*)malloc(size * ARRAYLENGTH * sizeof(float));
	//copy the old array elements to new
	for(int i=0;i<size*ARRAYLENGTH/2;i++){
		updateArray[i] = prev[i];
	}
	//free the space in the old array
	free(prev);
	return updateArray;
}

// Read the array from the file and return the array
float* readfile(char *filename,int *capacity,int *arraySize){
	FILE *filePointer = fopen (filename, "r");
	if (filePointer){
		float *arr;
		
		int i = 0;
	
		int length = 1;
		
		int numOfElements = 0;
		
		arr = (float*)malloc(ARRAYLENGTH * sizeof(float)); 

		while(fscanf(filePointer, "%f", arr)==1){
			
			if(i == length * ARRAYLENGTH - 1){
				//twice the size of the current full one
				length = length * 2;
				arr = arr - numOfElements;
				arr = updateArraySize(arr,length);
				//set the address to the end in order to add new data to the end of the array
				arr = arr + i;
				i = 0;
			}
			arr++;
			i++;
			numOfElements++;
		}
		//set the address back to the first place
		arr = arr - numOfElements;

		*arraySize = numOfElements;
		int cap = length * ARRAYLENGTH;
		*capacity = cap;

		return arr;
	}
	else{
		printf("Can't open the file! The file name is %s\n",filename);
	}
	fclose(filePointer);
}
