#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "readfile.h"

//Function declarations
double calculateMean(float *initialArray,int arraySize);
void sortArray(float *initialArray, int arraySize);
float calculateMedian(float *initialArray,int arraySize);
double calculateStddev(float *initialArray, int arraySize);

int main(int argc, char *argv[])
{
	if(argc!=2) {
		printf("Filename Error\n");
		return 0;
	}
    float *initialArray;
	int cap;
	int arraySize;
	

	printf("Results:\n");
	printf("-------------------------------------------\n");
	initialArray = readfile(argv[1],&cap,&arraySize);
	printf("Num values:\t%d\n",arraySize);
	double mean = calculateMean(initialArray,arraySize);
	printf("mean:\t\t%.3f\n",mean);
	float median = calculateMedian(initialArray,arraySize);
	printf("median:\t\t%.3f\n",median);
	double stddev = calculateStddev(initialArray,arraySize);
	printf("stddev:\t\t%.3f\n",stddev);
	printf("Unused array capacity: %d\n",cap - arraySize);
    printf("-------------------------------------------\n");
	free(initialArray);
	return 0;
}

// Return the mean of the array by passing the array and its size
double calculateMean(float *initialArray,int arraySize){
	double sum = 0.0;
    int i=0;
	while(i<arraySize){
		sum = sum + initialArray[i];
        i++;
	}
    
    double res = sum/arraySize;
	return res;
}

// Comparator to compare values
int comparator(const void *p, const void *q)
{
    // Get the values at given addresses
    int a = *(const int *)p;
    int b = *(const int *)q;
    return (a-b);
}

//Using qsort to sort the array to find the median
void sortArray(float *initialArray,int arraySize){
       int size = sizeof(initialArray) / sizeof(initialArray[0]);

       qsort((void*)initialArray, size, sizeof(initialArray[0]), comparator);
 
}


// Return the median of the array by passing the array and its size
float calculateMedian(float *initialArray,int arraySize){
	sortArray(initialArray,arraySize);
    // check for even case
	if(arraySize%2 != 0){
		return initialArray[(int)(arraySize/2)];
	}else{
		
        return (initialArray[(int)(arraySize/2)-1] + initialArray[(int)(arraySize/2)])/2;
	}
}


// Return the stddev of the array by passing the array and its size
double calculateStddev(float *initialArray, int arraySize){
	double mean = calculateMean(initialArray,arraySize);
	double sum = 0.0;
	for(int i=0;i<arraySize;i++){
		sum = sum + pow(initialArray[i]-mean , 2) ;
	}
    double res = sqrt(sum/arraySize);
	return res; 
}